package test.pack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayTest {
	public static void main(String[] args) {
		exOneArrays();
		exTwoArrays();
		exThreeArrays();
		exFourArrays();
		exFiveArrays();
		exSixArrays();
		exSevenArrays();
		exEightArrays();
		exNineArrays();
		exTenArrays();
		exElevenArrays();
		exTwelveArrays();
		exThirteenArrays();
		// exFourteenArrays();
		exFifteenArrays();
		exSixteenArrays();
		exSeventeenArrays();
		exEighteenArrays();
		// exNineteenArrays();
	}

	// 1. Write a Java program to remove duplicate elements from an array.
	private static void exOneArrays() {
		System.out.println("1. Write a Java program to remove duplicate elements from an array.");
		Object[] arr = { 1, 3, 2, 3, 5, 1, 6 };
		System.out.print("input arr:");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(" " + arr[i]);
		}
		System.out.println();

		removeDuplicateEl(arr);

		System.out.print("result arr:");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(" " + arr[i]);
		}
		System.out.println();
	}

	private static void removeDuplicateEl(Object[] arr) {
		if (arr.length == 0 || arr.length == 1) {
			return;
		}
		Arrays.sort(arr);
		Object[] temp = new Object[arr.length];
		temp[0] = arr[0];
		int j = 1;
		for (int i = 1; i < arr.length; i++) {
			if (temp[j - 1] != arr[i]) {
				temp[j] = arr[i];
				j++;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			arr[i] = temp[i];
		}
	}

	// 2. Write a Java program to find the second largest element in an array.
	private static void exTwoArrays() {
		System.out.println("2. Write a Java program to find the second largest element in an array.");
		Integer[] arr = { 1, 3, 2, 7, 5, 10, 6 };
		System.out.print("input arr:");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(" " + arr[i]);
		}
		System.out.println();

		Integer secondLargestEl = findSecondLargestEl(arr);

		if (secondLargestEl == null) {
			System.out.println("Wrong array length.");
		} else {
			System.out.println("The second largest element is: " + secondLargestEl);
		}

	}

	private static Integer findSecondLargestEl(Integer[] arr) {
		if (arr.length == 0 || arr.length == 1) {
			return null;
		}
		Arrays.sort(arr);
		int largestElement = arr[arr.length - 1];
		for (int i = arr.length - 1; i > 0; i--) {
			if (arr[i] < largestElement) {
				return arr[i];
			}
		}
		return largestElement;
	}

	// 3. Write a Java program to find the second smallest element in an array.
	private static void exThreeArrays() {
		System.out.println("3. Write a Java program to find the second smallest element in an array.  ");
		Integer[] arr = { 1, 3, 2, 7, 5, 10, 6 };
		System.out.print("input arr:");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(" " + arr[i]);
		}
		System.out.println();

		Integer secondSmallestEl = findSecondSmallestEl(arr);

		if (secondSmallestEl == null) {
			System.out.println("Wrong array length.");
		} else {
			System.out.println("The second smallest element is: " + secondSmallestEl);
		}
	}

	private static Integer findSecondSmallestEl(Integer[] arr) {
		if (arr.length == 0 || arr.length == 1) {
			return null;
		}
		Arrays.sort(arr);
		int smallestElement = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > smallestElement) {
				return arr[i];
			}
		}
		return smallestElement;
	}

	// 4. Write a Java program to convert an array to ArrayList.
	private static void exFourArrays() {
		System.out.println("4. Write a Java program to convert an array to ArrayList.");
		Integer[] arr = { 1, 3, 2, 7, 5, 10, 6 };
		List<Integer> arrAsList = Arrays.asList(arr);
		System.out.println("ArrayList: " + arrAsList.toString());
	}

	// 5. Write a Java program to convert an ArrayList to an array.
	private static void exFiveArrays() {
		System.out.println("5. Write a Java program to convert an ArrayList to an array.");
		List<Integer> arrAsList = Arrays.asList(1, 3, 2, 7, 5, 10, 6);
		Integer[] arr = (Integer[]) arrAsList.toArray();
		printArray("Array:", arr);
	}

	// 6. Write a Java program to find all pairs of elements in an array whose sum
	// is equal to a specified number.
	private static void exSixArrays() {
		System.out.println(
				"6. Write a Java program to find all pairs of elements in an array whose sum is equal to a specified number.");
		Integer specifiedNr = 9;
		System.out.println("Specified number is: " + specifiedNr);
		Integer[] arr = { 1, 3, 2, 7, 5, 10, 6 };
		printArray("Array:", arr);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length; j++) {
				if (i != j && (arr[i] + arr[j]) == specifiedNr) {
					System.out.println(arr[i] + " + " + arr[j] + " = " + specifiedNr);
				}
			}
		}
	}

	// 7. Write a Java program to test the equality of two arrays.
	private static void exSevenArrays() {
		System.out.println("7. Write a Java program to test the equality of two arrays.");
		Integer[] arr1 = new Integer[] { 20, 5, -95, 43, 17 };
		printArray("Array 1:", arr1);
		Integer[] arr2 = new Integer[] { 20, 5, -95, 43, 17 };
		printArray("Array 2 :", arr2);
		System.out.println(Arrays.equals(arr1, arr2));
	}

	// 8. Write a Java program to find a missing number in an array
	private static void exEightArrays() {
		System.out.println("8. Write a Java program to find a missing number in an array");
		Integer[] arr = new Integer[] { 6, 3, 5, 2, 1 };
		printArray("Array:", arr);
		Integer missingNr = findMissingNr(arr);
		System.out.println("missing number: " + missingNr);
	}

	private static Integer findMissingNr(Integer[] arr) {
		if (arr.length == 0 || arr.length == 1) {
			return null;
		}
		Arrays.sort(arr);
		int j = arr[0];
		for (Integer i : arr) {
			if (i != j) {
				return j;
			}
			j++;
		}
		return null;
	}

	// 9. Write a Java program to find common elements from three sorted (in
	// non-decreasing order) arrays
	private static void exNineArrays() {
		System.out.println(
				"9. Write a Java program to find common elements from three sorted (in non-decreasing order) arrays");
		Integer[] arr1 = { 1, 3, 5, 7, 9 };
		printArray("Array 1:", arr1);
		Integer[] arr2 = { 2, 3, 4, 5 };
		printArray("Array 2:", arr2);
		Integer[] arr3 = { 3, 4, 5, 6, 7, 8 };
		printArray("Array 3:", arr3);
		findEqualElements(arr1, arr2, arr3);
	}

	private static void findEqualElements(Integer[] arr1, Integer[] arr2, Integer[] arr3) {
		List<Integer> common = new ArrayList<>();
		int x = 0, y = 0, z = 0;
		while (x < arr1.length && y < arr2.length && z < arr3.length) {
			if (arr1[x] == arr2[y] && arr2[y] == arr3[z]) {
				common.add(arr1[x]);
				x++;
				y++;
				z++;
			} else if (arr1[x] < arr2[y]) {
				x++;
			} else if (arr2[y] < arr3[z]) {
				y++;
			} else {
				z++;
			}
		}
		System.out.println("Common elements from three sorted (in non-decreasing order ) arrays: " + common);
	}

	// 10. Write a Java program to move all 0's to the end of an array. Maintain the
	// relative order of the other (non-zero) array elements.
	private static void exTenArrays() {
		System.out.println(
				"10. Write a Java program to move all 0's to the end of an array. Maintain the relative order of the other (non-zero) array elements.");
		Integer[] arr = { 0, 0, 1, 2, 3, 0, 4, 0, 5, 6, 7 };
		printArray("Array:", arr);
		int i = 0;
		for (int j = 0, l = arr.length; j < l; j++) {
			if (arr[j] == 0) {
			} else {
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				i++;
			}
		}
		for (; i < arr.length; ++i) {
			arr[i] = 0;
		}
		printArray("Moved zeroes array:", arr);
	}

	// 11. Write a Java program to find the number of even and odd integers in a
	// given array of integers.
	private static void exElevenArrays() {
		System.out.println(
				"11. Write a Java program to find the number of even and odd integers in a given array of integers.");
		Integer[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		printArray("Array:", arr);
		int evenIntegers = 0;
		int oddIntegers = 0;
		for (int i : arr) {
			if (i % 2 == 0) {
				evenIntegers++;
			} else {
				oddIntegers++;
			}
		}
		System.out.println("EvenIntegers are: " + evenIntegers + ", OddIntegers are: " + oddIntegers);
	}

	// 12. Write a Java program to get the difference between the largest and
	// smallest values in an array of integers. The length of the array must be 1
	// and above.
	private static void exTwelveArrays() {
		System.out.println("12. Write a Java program to get the difference between the largest and smallest "
				+ "values in an array of integers. The length of the array must be 1 and above.");
		Integer[] arr = { -1, 0, 1, 5, 3, 2, 4, 6 };
		printArray("Array:", arr);
		int maxVal = arr[0];
		int minVal = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] > maxVal)
				maxVal = arr[i];
			else if (arr[i] < minVal)
				minVal = arr[i];
		}
		System.out.println("Difference between the largest and smallest values of the array is: " + (maxVal - minVal));
	}

	// 13. Write a Java program to compute the average value of an array of integers
	// except the largest and smallest values.
	private static void exThirteenArrays() {
		System.out.println("13. Write a Java program to compute the average value"
				+ " of an array of integers except the largest and smallest values.");
		Integer[] arr = { 1, 7, 5, 2, 4, 3, 6 };
		printArray("Array:", arr);
		int maxVal = arr[0];
		int minVal = arr[0];
		float sum = arr[0];
		for (int i = 1; i < arr.length; i++) {
			sum += arr[i];
			if (arr[i] > maxVal) {
				maxVal = arr[i];
			} else if (arr[i] < minVal) {
				minVal = arr[i];
			}
		}
		float x = (sum - (maxVal + minVal)) / (arr.length - 2);
		System.out.println("Average value of an array of integers is: " + x);
	}

	// 14. Write a Java program to check if an array of integers without 0 and -1.
	private static void exFourteenArrays() {
		// TODO Ne razbiram uslovieto. Ima se predvid na zadaden masiv da se proveri
		// dali sudurja 0 ili -1 li ?
	}

	// 15. Write a Java program to check if the sum of all the 10's in the array is
	// exactly 30.
	// Return false if the condition does not satisfy, otherwise true.
	private static void exFifteenArrays() {
		System.out.println("15. Write a Java program to check if the sum of all the 10's in the array is exactly 30."
				+ " Return false if the condition does not satisfy, otherwise true.");
		Integer[] arr = { 1, 10, 5, 2, 10, 3, 10 };
		printArray("Array:", arr);
		int countOfTen = 0;
		for (int i : arr) {
			if (i == 10) {
				countOfTen++;
			}
		}
		System.out.println(countOfTen * 10 == 30 ? true : false);
	}

	// 16. Write a Java program to check if an array of integers contains two
	// specified elements 65 and 77.
	private static void exSixteenArrays() {
		System.out.println(
				"16. Write a Java program to check if an array of integers contains two specified elements 65 and 77.");
		Integer[] arr = { 1, 10, 65, 2, 10, 3, 10, 77 };
		printArray("Array:", arr);
		int el1 = 65;
		int el2 = 77;
		boolean contEl1 = false;
		boolean contEl2 = false;
		for (int i : arr) {
			if (i == el1) {
				contEl1 = true;
			} else if (i == el2) {
				contEl2 = true;
			}
		}
		System.out.println(contEl1 && contEl2);
	}

	// 17. Write a Java program to remove the duplicate elements of a given array
	// and return the new length of the array.
	private static void exSeventeenArrays() {
		System.out.println(
				"17. Write a Java program to remove the duplicate elements of a given array and return the new length of the array.");
		Integer[] arr = { 20, 20, 30, 40, 50, 50, 50 };
		printArray("Array:", arr);
		int newSize = removeDuplicateEl(arr);
		System.out.println("New size is: " + newSize);
	}

	private static int removeDuplicateEl(Integer[] arr) {
		if (arr.length == 0 || arr.length == 1) {
			return arr.length;
		}
		Arrays.sort(arr);
		Integer[] temp = new Integer[arr.length];
		temp[0] = arr[0];
		int j = 1;
		for (int i = 1; i < arr.length; i++) {
			if (temp[j - 1] != arr[i]) {
				temp[j] = arr[i];
				j++;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			arr[i] = temp[i];
		}
		return j--;
	}

	// 18. Write a Java program to find the length of the longest consecutive
	// elements sequence from a given unsorted array of integers.
	private static void exEighteenArrays() {
		System.out.println("18. Write a Java program to find the length of the longest "
				+ "consecutive elements sequence from a given unsorted array of integers.");
		Integer[] arr = { 49, 1, 3, 200, 2, 4, 70, 5 };
		printArray("Array:", arr);
		findLongestSequence(arr);

	}

	private static void findLongestSequence(Integer[] arr) {
		Integer[] tempArr = Arrays.copyOf(arr, arr.length);
		Arrays.sort(tempArr);
		int ans = 0, count = 0;
		int lastIndex = 0;
		for (int i = 0; i < tempArr.length; i++) {
			if (i > 0 && tempArr[i] == tempArr[i - 1] + 1) {
				count++;
			} else {
				int tempAns = Math.max(ans, count);
				if (tempAns > ans) {
					lastIndex = i;
					ans = tempAns;
				}
				count = 1;
			}
		}
		Integer[] finalArr = new Integer[ans];
		for (int j = lastIndex - 1, i = 0; j >= (lastIndex - ans); j--, i++) {
			finalArr[i] = tempArr[j];
		}
		Arrays.sort(finalArr);
		System.out.println("The longest consecutive elements sequence is " + Arrays.toString(finalArr)
				+ ", therefore the program will return its length " + ans);

	}

	// 19. Write a Java program to find the sum of the two elements of a given array
	// which is equal to a given integer.
	private static void exNineteenArrays() {
		// TODO ne shvashtam s kakvo e po-razlichna ot zadacha nomer 6, kojto e po-gore.
		// Moje bi uslovieto ne e zadadeno mnogo korektno.

	}

	private static void printArray(String text, Integer[] arr) {
		System.out.print(text);
		for (int i : arr) {
			System.out.print(" " + i);
		}
		System.out.println();
	}
}
